package gostaticfilehandler

import (
	"net/http"
	"path/filepath"
	"strings"
)

//ContentTypes is map that register the not common context-type. key is file extension and value is content-type.
var ContentTypes map[string]string

//HandleResourceFiles start to binding handler for static resource files.
func HandleResourceFiles(pattern, physicalRoot string) {
	//パターンは末尾スラッシュである必要あり。スラッシュが無い場合は pattern以下を再帰的に紐づけることができない。(pattern/**にマッチしない)
	//path.Joinでは末尾にスラッシュを追加することができない為、文字列連結している。
	if !strings.HasSuffix(pattern, "/") {
		pattern = pattern + "/"
	}
	handler := &resourceHandler{pattern, physicalRoot}
	http.HandleFunc(pattern, handler.onRequest)
}

type resourceHandler struct {
	pattern      string
	physicalRoot string
}

func (h *resourceHandler) createPhysicalPath(reqPath string) string {
	splitedPathWithoutRootSlash := strings.Split(reqPath[1:], "/")
	stIdx := 1
	if h.pattern == "/" {
		stIdx = 0
	}
	return filepath.Join(h.physicalRoot, filepath.Join(splitedPathWithoutRootSlash[stIdx:]...))
}

func (h *resourceHandler) onRequest(w http.ResponseWriter, r *http.Request) {
	resPath := h.createPhysicalPath(r.URL.Path)
	http.ServeFile(w, r, resPath)
	if contentType, ok := ContentTypes[filepath.Ext(resPath)]; ok {
		w.Header().Set("content-type", contentType)
	}
}

func init() {
	ContentTypes = make(map[string]string)
	ContentTypes["woff"] = "application/font-woff"
}
