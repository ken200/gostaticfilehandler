package gostaticfilehandler

import "testing"

func TestCreatePhysicalPath(t *testing.T) {
	cases := []struct {
		pattern, physicalRoot, reqPath, expect string
	}{
		{"/resources", "resources", "/resources/js/hoge.js", "resources\\js\\hoge.js"},
		{"/resources", "resources", "/resources/hoge.txt", "resources\\hoge.txt"},

		{"/", "resources", "/js/hoge.js", "resources\\js\\hoge.js"},
		{"/", "resources", "/hoge.txt", "resources\\hoge.txt"},

		{"/resources", "", "/resources/js/hoge.js", "js\\hoge.js"},
		{"/resources", "", "/resources/hoge.txt", "hoge.txt"},
	}

	for i, c := range cases {
		rh := &resourceHandler{c.pattern, c.physicalRoot}
		result := rh.createPhysicalPath(c.reqPath)
		if result != c.expect {
			t.Logf("No.%d expect is %s, but result is %s \r\n", i+1, c.expect, result)
		}
	}
}
