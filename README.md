gostaticfilehandler
===

code snippet for static file handling.

## using package example

```
package main
import "bitbucket.org/ken200/gostaticfilehandler"

func main() {
	...
	gostaticfilehandler.HandleResourceFiles("/js", filepath.Join("resources", "js"))
	gostaticfilehandler.HandleResourceFiles("/css", filepath.Join("resources", "css"))
	gostaticfilehandler.HandleResourceFiles("/fonts", filepath.Join("resources", "fonts"))
	gostaticfilehandler.HandleResourceFiles("/", "pages")
	http.ListenAndServe(":8888", nil)
}
```

